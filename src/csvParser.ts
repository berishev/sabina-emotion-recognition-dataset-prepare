import * as csv from 'fast-csv';
import * as path from 'path';

export enum NeuralEmotionTypeEnum {
  Angry = 0,
  Disgust = 1,
  Fear = 2,
  Happy = 3,
  Sad = 4,
  Surprise = 5,
  Neutral = 6
}

export enum NeuralUsageTypeEnum {
  Training,
  PublicTest,
  PrivateTest
}

export interface NeuralItem {
  emotionType: NeuralEmotionTypeEnum;
  pixels: number[];
  usage: NeuralUsageTypeEnum;
}

function readRaw(fileName: string) {
  return new Promise<string[][]>((resolve, reject) => {
    let data: string[][] = [];
    csv
      .fromPath(path.join(__dirname, fileName))
      .on('data', function(res: string[]) {
        if (res instanceof Array) {
          data.push(res);
        }
      })
      .on('end', () => resolve(data))
      .on('error', err => reject(err));
  });
}

export default async function() {
  console.log('start read file');
  let name = '../fer2013.csv';
  let data = await readRaw(name);
  console.log('file readed');
  let outputData: NeuralItem[] = [];
  let headers = data.shift();
  console.log('prepare data');
  for (let item of data) {
    try {
      let emotionString = item[0];
      let pixelsString = item[1];
      let usageString = item[2];
      if (emotionString != null && pixelsString != null && usageString != null) {
        let emotionType = Number.parseInt(emotionString) as NeuralEmotionTypeEnum;
        let pixels = pixelsString.split(' ').map(m => Number.parseInt(m));
        let usage = NeuralUsageTypeEnum[usageString] || NeuralUsageTypeEnum.Training;
        if (pixels.length == 2304) {
          outputData.push({
            emotionType,
            pixels,
            usage
          });
        }
      }
    } catch (err) {
      console.log(err);
    }
  }
  console.log('data successful prepared');
  return outputData;
}
