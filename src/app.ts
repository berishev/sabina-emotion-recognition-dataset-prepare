import csvParser, { NeuralUsageTypeEnum, NeuralEmotionTypeEnum, NeuralItem } from './csvParser';
import * as fs from 'fs';
import * as path from 'path';

export enum NeuralEmotionRealTypeEnum {
  Angry = 0,
  Happy = 1,
  Sad = 2,
  Neutral = 3
}

export default class App {
  private static _instance: App = null;

  static get instance() {
    if (this._instance == null) this._instance = new App();
    return this._instance;
  }

  writeToJson(fileName: string, data: NeuralItem[], count: number) {
    let copyArray = [].concat(data) as NeuralItem[];
    let length = copyArray.length;
    let promises: Promise<void>[] = [];
    for (let i = 0, j = 0; i < length; j++) {
      let currentCount = length - i < count ? length - i : count;
      let currentData = copyArray.splice(0, currentCount);
      console.log(currentData.length, currentCount);
      let json = JSON.stringify(currentData);
      let jsonPromise = new Promise<void>((resolve, reject) =>
        fs.writeFile(path.join(__dirname, `/data/${fileName}${j}.json`), json, err => {
          if (err) return reject();
          console.log(`file ${fileName}${j}.json writed`);
          return resolve();
        })
      );
      promises.push(jsonPromise);
      i += currentCount;
    }
    return Promise.all(promises);
  }

  async run() {
    let data = await csvParser();
    let trainingData = data;
    console.log('length items for training', trainingData.length);
    await this.writeToJson('data', trainingData, 500);
  }
}
